-- phpMyAdmin SQL Dump
-- version 4.7.0
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Oct 20, 2017 at 05:12 PM
-- Server version: 10.1.25-MariaDB
-- PHP Version: 7.1.7

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `medb`
--

-- --------------------------------------------------------

--
-- Table structure for table `email_forget`
--

CREATE TABLE `email_forget` (
  `id` int(11) NOT NULL,
  `token` text COLLATE utf8_persian_ci NOT NULL,
  `cread_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `email` text COLLATE utf8_persian_ci NOT NULL,
  `exp_forget` tinyint(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_persian_ci;

--
-- Dumping data for table `email_forget`
--

INSERT INTO `email_forget` (`id`, `token`, `cread_at`, `email`, `exp_forget`) VALUES
(4, 'XZa2vxhHRp17zEZ', '2017-10-16 15:11:43', 'maysammm@gmail.com', 1);

-- --------------------------------------------------------

--
-- Table structure for table `goftego`
--

CREATE TABLE `goftego` (
  `id` int(11) NOT NULL,
  `username` text COLLATE utf8_persian_ci,
  `date` text COLLATE utf8_persian_ci,
  `title` text COLLATE utf8_persian_ci,
  `content` text COLLATE utf8_persian_ci NOT NULL,
  `cread_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_persian_ci;

--
-- Dumping data for table `goftego`
--

INSERT INTO `goftego` (`id`, `username`, `date`, `title`, `content`, `cread_at`) VALUES
(7, 'meisam.moh', '۱۳۹۶/۷/۲۶', 'API چیست؟', '<h3><a href=\"https://9px.ir/learn/\" style=\"color: gray; font-size: 0.75em; font-weight: normal;\">آموزش&zwnj;ها</a></h3>\r\n\r\n<p><span style=\"color:gray\">&raquo;</span></p>\r\n\r\n<h1><a href=\"https://9px.ir/learn/all-things-about-api/\">API چیست؟</a></h1>\r\n\r\n<div class=\"clr\">&nbsp;</div>\r\n\r\n<div class=\"clr\">&nbsp;</div>\r\n\r\n<p>API مخفف application programming interface (رابط برنامه&zwnj;نویسی نرم&zwnj;افزار) واسطه&zwnj;ای است بین یک کتابخانه با برنامه&zwnj;هایی که از آن کتابخانه تقاضای سرویس می&zwnj;کنند.<br />\r\nبه صورت ساده و قابل فهم، API ها مثل function هایی هستند که در یک سرور (یا سیستم عامل) وجود دارد و یک برنامه نویس در برنامه خود می&zwnj;تواند آنها را فراخوانی و استفاده کند بدون اینکه به سورس و نحوه عملکرد آن دسترسی داشته باشد.<br />\r\nبه عنوان مثال ویندوز یک سیستم عامل کدبسته (غیر اپن سورس) است اما مایکروسافت برای ویندوز، APIهایی قرار داده است که با استفاده از آنها، برنامه&zwnj;نویسان می&zwnj;توانند از قابلیت&zwnj;ها و سرویس&zwnj;های سیستم&zwnj;عامل ویندوز در توسعه و نوشتن برنامه&zwnj;های کاربردی خود استفاده کنند.</p>\r\n\r\n<h3>API های تحت وب</h3>\r\n\r\n<p>API های تحت وب به مراتب گسترده&zwnj;تر، پرکاربردتر و از همه مهم&zwnj;تر؛ مستقل از سیستم&zwnj;عامل است. یعنی یک برنامه ممکن است روی سیستم عامل linux باشد اما شما که یک سرور ویندوزی دارید، می&zwnj;توانید از API آن به سادگی استفاده کنید.<br />\r\nSOAP و REST دو گروه معروف از API های تحت وب هستند که دومی ساده&zwnj;تر و استفاده از آن آسان&zwnj;تر است. اگر شما کتاب <a href=\"http://www.wowebook.com/book/restful-web-apis/\" target=\"_blank\">RESTful Web APIs</a> را که تازه چاپ شده، دانلود و مطالعه بفرمایید، بیشتر با آن آشنا می&zwnj;شوید.</p>\r\n\r\n<div class=\"c\"><img alt=\"API - واسط برنامه نویسی\" src=\"https://9px.ir/i/api.png\" /></div>\r\n\r\n<p>سایت&zwnj;های پربازدید و شبکه&zwnj;های اجتماعی معمولا API دارند که موجب می&zwnj;شود برنامه (اپلیکیشن)های زیادی برای تعامل با آنها (نمایش/جستجو/نوشتن) در سیستم&zwnj;عامل&zwnj;های مختلف ایجاد شود و معمولا تعداد بسیار زیادی از کاربران از همین طریق از شبکه&zwnj;های اجتماعی استفاده می&zwnj;کنند.</p>\r\n\r\n<h3>معرفی چند نمونه API</h3>\r\n\r\n<p>در <a href=\"http://www.webresourcesdepot.com/15-free-apis-you-didnt-hear-about-but-will-make-use-of/\" target=\"_blank\">این مطلب</a> می&zwnj;توانید 15 تا از API های ساده تا پیشرفته تحت وب را ببینید و از آن استفاده کنید. یکی از جذاب&zwnj;ترین API های معرفی شده در این لیست برای من API مربوط به <a href=\"https://cloudconvert.org/\" target=\"_blank\">تبدیل فایل</a> بود که به کمک آن می&zwnj;توانید هر نوع فایل (صوتی/تصویری/متنی/...) را به نوع دیگر تبدیل کنید!<br />\r\nمثلا می&zwnj;توانید یک فایل wma یا wav را به mp3 تبدیل کنید یا یک فیلم avi را به mp4 :)<br />\r\nیا می&zwnj;توانید یک فایل word تحویل بدهید و یک فایل txt تحویل بگیرید و ...<br />\r\nآپدیت: امروز سایت Encoding.com را دیدم که در پلن رایگان خود نیز امکانات خوبی برای تبدیل فایل&zwnj;های صوتی و تصویری به صورت آنلاین توسط صفحه وب و همچنین API فراهم آورده است که برای سایت&zwnj;های حاوی صوت و تصویر عالی است.</p>\r\n\r\n<p>در <a href=\"https://github.com/toddmotto/public-apis/blob/master/README.md\" target=\"_blank\">این صفحه گیت&zwnj;هاب</a> نیز لیست خوبی فراهم آمده است.&lt;&nbsp;--&nbsp;ادامه&nbsp;مطلب&nbsp;--!&gt;</p>\r\n\r\n<p><strong>API های سرویس آپلود فایل</strong>: به شخصه از این API ها زیاد استفاده کرده&zwnj;ام. خصوصا برای تهیه بکآپ اتوماتیک آنلاین.<br />\r\nبسیاری از سرویس&zwnj;های اشتراک فایل یا آپلودسنترهای معتبر، دارای یک رابط برنامه نویسی API برای developper ها هستند که به برنامه نویس امکان می&zwnj;دهد به سادگی فایل&zwnj;های موردنظرش را در آن سرویس آپلود کند. از جمله آنها می&zwnj;توان به filecloud.io و ge.tt و ... اشاره کرد.<br />\r\nناگفته نماند که برخی از اینگونه سایت&zwnj;ها، امکان آپلود از طریق ftp را نیز در اختیار کاربر قرار می&zwnj;دهند که کار را بسیار ساده&zwnj;تر می&zwnj;کند .از جمله این سایت&zwnj;ها نیز می&zwnj;توان <a href=\"http://uploading.com/\" target=\"_blank\">uploading.com</a> و <a href=\"http://archive.org/\" target=\"_blank\">archive.org</a> را نام برد.</p>\r\n\r\n<p><strong>API برای OCR: </strong>که البته بیشتر برای دور زدن <a href=\"https://9px.ir/note/bulletproof-captcha/\">کپچا</a>های ساده کاربرد دارد تا تبدیل متن اسکن شده به متن تایپ&zwnj;شده :)<br />\r\n<a href=\"https://www.idolondemand.com/developer/apis/ocrdocument#overview\" target=\"_blank\">این</a> یک نمونه سرویس رایگان (اما محدود به 5 هزار در ماه) برای OCR است که API هم دارد و <a href=\"http://hayageek.com/extract-text-from-image-php\" target=\"_blank\">این هم یک نمونه کد</a> برای کار با آن است. <a href=\"http://www.imagetyperz.com/\" target=\"_blank\">این سرویس</a> نیز هرچند رایگان نیست اما قیمت آن تقریبا معادل رایگان است!</p>\r\n\r\n<h3>چگونه از API استفاده کنیم؟</h3>\r\n\r\n<p>طبعا API برای برنامه&zwnj;نویسان تهیه شده و برای استفاده از آن لازم است در دانش برنامه&zwnj;نویسی اطلاعاتی داشته باشید. بسیاری از سایت&zwnj;هایی که دارای API هستند، به همراه آن مثال و نمونه&zwnj;کد آموزشی نیز ارائه می&zwnj;دهند.<br />\r\nدر <a href=\"http://www.webresourcesdepot.com/best-tutorials-for-learning-php-based-api-development/\" target=\"_blank\">این مطلب</a> نیز بیش از 16 مقاله در مورد استفاده از چند API ی معروف لیست شده است.</p>\r\n\r\n<h3>چگونه API موردنیازم را پیدا کنم؟</h3>\r\n\r\n<p>ممکن است شما برای کارتان به یک API نیاز داشته باشید. اولین راهی که برای یافتن API کارآمد به ذهن هر کس می&zwnj;رسد، جستجو در گوگل یا پرسش در استک&zwnj;اورفلو است. اما علاوه بر آن، شما می&zwnj;توانید از سایت&zwnj;هایی همچون <a href=\"http://www.mashape.com/\" target=\"_blank\">Mashape</a> و <a href=\"http://www.programmableweb.com/\" target=\"_blank\">ProgrammableWeb</a> نیز در این جهت استفاده کنید که انواع API های مختلف را لیست کرده&zwnj;اند...</p>\r\n\r\n<h3>چگونه برای سایت خودم API بنویسم؟</h3>\r\n\r\n<p>ممکن است فکر کنید که نوشتن یک API برای یک سایت سخت است اما اینطور نیست. نوشتن API آسان&zwnj;تر از نوشتن برنامه کامل یک سایت است زیرا در API صرفا قرار است حسب آدرس درخواستی، داده&zwnj;ها (اطلاعات) با ساختاری که از قبل تعیین کردید (مثلا JSON) به عنوان پاسخ آورده شود.</p>\r\n\r\n<p>همچنین همانطور که بخشی از یک سایت می&zwnj;تواند فقط مختص کاربران احراز هویت شده باشد، در API هم می&zwnj;تواند همه یا بخشی از آن مختص کاربرانی باشد که احراز هویت شده&zwnj;اند. البته یک نکته ظریف وجود دارد و آن اینکه در API نمی&zwnj;توان <a href=\"https://9px.ir/note/bulletproof-captcha/\">کپچا</a> گذاشت چون API ها توسط برنامه&zwnj;ها (و نه انسان) استفاده می&zwnj;شود. لذا API های متداول، به جای user, password از یک کلید رندوم مثلا 50 کاراکتری به نام API key استفاده می&zwnj;کنند که تشخیص آن عملا غیرممکن است (برخلاف پسوردها که معمولا کوتاه است و با حمله brute force می&zwnj;توان آن را حدس زد). ضمن اینکه در این روش، پسورد کاربر محفوظ می&zwnj;ماند و همچنین با API key های مختلف، می&zwnj;توان نظارت و کنترل بهتری روی برنامه استفاده&zwnj;کننده از API سایت&zwnj;مان داشته باشیم و در صورت نیاز موارد مشکوک یا پرمصرف را مسدود یا غیررایگان کنیم.</p>\r\n\r\n<p>اگر هنوز در برنامه&zwnj;نویسی با PHP چندان مسلط نیستید، کتابخانه&zwnj;های PHP وجود دارد که برای شروع API نویسی، نمونه خوبی هستند مثلا این <a href=\"https://github.com/martinbean/api-framework\" target=\"_blank\">فریم&zwnj;ورک مخصوص API از آقای martin bean</a> را ببینید.</p>\r\n\r\n<p>در مورد <a href=\"https://9px.ir/note/all-methods-for-identifying-client-from-a-webpage/\">احراز هویت کاربر </a>نیز قبلا مطلبی نوشتم و گفته شد که بجای روش مرسوم و قدیمی و پرخطر cookie بهتر است از روش&zwnj;های token based استفاده کنید که jwt یک نمونه آن است.</p>\r\n', '2017-10-18 19:44:00'),
(8, 'meisam.moh', '۱۳۹۶/۷/۲۶', 'API چیست؟', '<p>API مخفف application programming interface (رابط برنامه&zwnj;نویسی نرم&zwnj;افزار) واسطه&zwnj;ای است بین یک کتابخانه با برنامه&zwnj;هایی که از آن کتابخانه تقاضای سرویس می&zwnj;کنند.<br />\r\nبه صورت ساده و قابل فهم، API ها مثل function هایی هستند که در یک سرور (یا سیستم عامل) وجود دارد و یک برنامه نویس در برنامه خود می&zwnj;تواند آنها را فراخوانی و استفاده کند بدون اینکه به سورس و نحوه عملکرد آن دسترسی داشته باشد.<br />\r\nبه عنوان مثال ویندوز یک سیستم عامل کدبسته (غیر اپن سورس) است اما مایکروسافت برای ویندوز، APIهایی قرار داده است که با استفاده از آنها، برنامه&zwnj;نویسان می&zwnj;توانند از قابلیت&zwnj;ها و سرویس&zwnj;های سیستم&zwnj;عامل ویندوز در توسعه و نوشتن برنامه&zwnj;های کاربردی خود استفاده کنند.</p>\r\n\r\n<h3>API های تحت وب</h3>\r\n\r\n<p>API های تحت وب به مراتب گسترده&zwnj;تر، پرکاربردتر و از همه مهم&zwnj;تر؛ مستقل از سیستم&zwnj;عامل است. یعنی یک برنامه ممکن است روی سیستم عامل linux باشد اما شما که یک سرور ویندوزی دارید، می&zwnj;توانید از API آن به سادگی استفاده کنید.<br />\r\nSOAP و REST دو گروه معروف از API های تحت وب هستند که دومی ساده&zwnj;تر و استفاده از آن آسان&zwnj;تر است. اگر شما کتاب <a href=\"http://www.wowebook.com/book/restful-web-apis/\" target=\"_blank\">RESTful Web APIs</a> را که تازه چاپ شده، دانلود و مطالعه بفرمایید، بیشتر با آن آشنا می&zwnj;شوید</p>\r\n', '2017-10-18 19:43:51'),
(9, 'meisam.moh', '۱۳۹۶/۷/۲۶', 'ایران کوروش در 2000 سال پیش', '<p>ایرانیان در سال&nbsp;&nbsp; ها پیش&nbsp; در کشور ایران به فرمانروایی کوروش کبیر زیر اسمان&nbsp; ایران&nbsp; زندگی&nbsp; می کردن&nbsp; و از شرایط زندگی&nbsp; خود&nbsp; راضی&nbsp; بودن&nbsp; و&nbsp; با&nbsp; مسائل&nbsp; جامع&nbsp;</p>\r\n\r\n<p>مشکلی&nbsp; خاصی&nbsp; نداشتن&nbsp;</p>\r\n', '2017-10-18 20:09:15');

-- --------------------------------------------------------

--
-- Table structure for table `nazar`
--

CREATE TABLE `nazar` (
  `id` int(11) NOT NULL,
  `username` text COLLATE utf8_persian_ci,
  `nazar` text COLLATE utf8_persian_ci,
  `name` text COLLATE utf8_persian_ci,
  `email` text COLLATE utf8_persian_ci,
  `date` text COLLATE utf8_persian_ci,
  `caredte_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_persian_ci;

--
-- Dumping data for table `nazar`
--

INSERT INTO `nazar` (`id`, `username`, `nazar`, `name`, `email`, `date`, `caredte_at`) VALUES
(1, 'میمان', 'aghkajsfghajhfghj', 'maysammmm', 'maysammmm@djdklhjfk', '۱۳۹۶/۷/۲۶', '2017-10-18 22:48:45');

-- --------------------------------------------------------

--
-- Table structure for table `token`
--

CREATE TABLE `token` (
  `id` int(11) NOT NULL,
  `email` text COLLATE utf8_persian_ci NOT NULL,
  `username` text COLLATE utf8_persian_ci NOT NULL,
  `password` text COLLATE utf8_persian_ci NOT NULL,
  `token` text COLLATE utf8_persian_ci NOT NULL,
  `ip` text COLLATE utf8_persian_ci NOT NULL,
  `date_exp` text COLLATE utf8_persian_ci NOT NULL,
  `caredte_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_persian_ci;

--
-- Dumping data for table `token`
--

INSERT INTO `token` (`id`, `email`, `username`, `password`, `token`, `ip`, `date_exp`, `caredte_at`) VALUES
(1, 'maysammm@gmail.com', 'meisam1982', '321321', 'f9IsrhEdTAWG2BS', '::1', '1510257209', '2017-10-16 15:10:10'),
(2, 'maysammm@gmail.com', 'meisam1983', '321321', 'wiQ9bK8v79cY5n4', '::1', '1510267175', '2017-10-16 15:10:10'),
(3, 'mohammadi', 'meisam', '123456', 'Iva439NItlnIqPo', '::1', '1510292123', '2017-10-10 08:05:23'),
(4, 'aaa', 'ali111', '0073551198', 'TWgw2SJXM8czKGn', '::1', '1510292206', '2017-10-16 09:16:21'),
(5, 'maysammm@gmail.com', 'meisam1363', '321321', 'nYGoWR0qkmSuwqA', '::1', '1510316035', '2017-10-16 15:10:10'),
(6, 'mm', '123456', '123456', '85mK4k5ZoEvcthR', '::1', '1510384107', '2017-10-11 09:38:27'),
(7, 'maysammm', 'mmsswwxxxxx', '1123456888', 'VVJXVhlxf2SwKsq', '::1', '1510415970', '2017-10-11 18:29:30'),
(8, 'maysammm', 'mmsswwxxxxx', '1123456888', 'HhTIPFAYJuwpSJm', '::1', '1510415994', '2017-10-11 18:29:54'),
(9, 'maysammm', 'mmsswwxxxxx', '1123456888', '6GK2uziLMOeEwH3', '::1', '1510416111', '2017-10-11 18:31:51'),
(10, 'maysammm', 'mmsswwxxxxx', '11111111', 'hZTh08uIogQnPmr', '::1', '1510416497', '2017-10-11 18:38:17'),
(11, 'mm', '11', '111111', '4pICXuGwxuReL67', '::1', '1510416618', '2017-10-11 18:40:18'),
(12, 'mm', '11', '111111', 'wWBsj85SHkX4Ewl', '::1', '1510416631', '2017-10-11 18:40:31'),
(13, 'mm', '11', 'aaaaaaa', 'IJZyZnvQ6eCzh2w', '::1', '1510417081', '2017-10-11 18:48:01'),
(14, 'mmasyaamsd', 'meisam.moh', '0073551198', 'MUWFPvLJRpdf9XH', '::1', '1510418330', '2017-10-11 19:08:50'),
(15, 'maysammmmujwbhsg', 'meisammmmjjj', '0073551198', 'uAUKIdndcMH8eDo', '::1', '1510418424', '2017-10-11 19:10:24'),
(16, 'maysammm@gmail.com', 'meisam.moh1236', '321321', 'Al2IVikAlD62myd', '::1', '1510418503', '2017-10-16 15:10:10'),
(17, 'mei@hhhh', 'meisam1363', '70b5bfaed6846418327aa667b1f1b8a3', 'F3O7ECllrDJb4bt', '::1', '1511082229', '2017-10-19 11:33:49'),
(18, 'naysammm@gmail.com', 'meisam1360', '70b5bfaed6846418327aa667b1f1b8a3', '0cuFTzLqZ2QAI9K', '::1', '1511118525', '2017-10-19 21:38:45'),
(21, 'maysammm@gmail.com', 'meisam1359', '70b5bfaed6846418327aa667b1f1b8a3', '6LuNeppr6dRWc8T', '::1', '1511190244', '2017-10-20 17:34:04');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `user_id` int(11) NOT NULL,
  `user_name` varchar(15) NOT NULL,
  `user_email` varchar(40) NOT NULL,
  `user_pass` varchar(255) NOT NULL,
  `joining_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `vist`
--

CREATE TABLE `vist` (
  `id` int(11) NOT NULL,
  `amar_vist` text COLLATE utf8_persian_ci,
  `ip` text COLLATE utf8_persian_ci,
  `id_rss` int(11) DEFAULT NULL,
  `caredte_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_persian_ci;

--
-- Dumping data for table `vist`
--

INSERT INTO `vist` (`id`, `amar_vist`, `ip`, `id_rss`, `caredte_at`) VALUES
(1, '1', '::1', NULL, '2017-10-18 23:19:40'),
(2, '1', '::1', NULL, '2017-10-18 23:19:56'),
(3, '1', '::1', NULL, '2017-10-18 23:20:00'),
(4, '1', '::1', NULL, '2017-10-18 23:20:33'),
(5, '1', '::1', NULL, '2017-10-18 23:20:37'),
(6, '1', '::1', 7, '2017-10-19 09:20:04'),
(7, '1', '::1', 8, '2017-10-19 10:16:09'),
(8, '1', '::1', 8, '2017-10-19 10:18:26'),
(9, '1', '::1', 8, '2017-10-19 10:19:13'),
(10, '1', '::1', 7, '2017-10-19 11:13:25'),
(11, '1', '::1', 9, '2017-10-19 11:13:32'),
(12, '1', '::1', 9, '2017-10-19 11:13:36'),
(13, '1', '::1', 7, '2017-10-19 21:53:17'),
(14, '1', '::1', NULL, '2017-10-19 22:11:25'),
(15, '1', '::1', NULL, '2017-10-19 22:12:05'),
(16, '1', '::1', NULL, '2017-10-19 22:12:22'),
(17, '1', '::1', 7, '2017-10-19 22:12:50'),
(18, '1', '::1', NULL, '2017-10-19 22:12:59'),
(19, '1', '::1', NULL, '2017-10-19 22:13:21'),
(20, '1', '::1', 7, '2017-10-19 22:13:37'),
(21, '1', '::1', NULL, '2017-10-19 22:16:12'),
(22, '1', '::1', 7, '2017-10-19 22:16:17'),
(23, '1', '::1', NULL, '2017-10-19 22:16:26');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `email_forget`
--
ALTER TABLE `email_forget`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `goftego`
--
ALTER TABLE `goftego`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `nazar`
--
ALTER TABLE `nazar`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `token`
--
ALTER TABLE `token`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`user_id`);

--
-- Indexes for table `vist`
--
ALTER TABLE `vist`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `email_forget`
--
ALTER TABLE `email_forget`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT for table `goftego`
--
ALTER TABLE `goftego`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;
--
-- AUTO_INCREMENT for table `nazar`
--
ALTER TABLE `nazar`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `token`
--
ALTER TABLE `token`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=22;
--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `user_id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `vist`
--
ALTER TABLE `vist`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=24;COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
