﻿<?php

require_once("public.php");

if(!empty($_SESSION['user_session'])){
echo "<meta http-equiv=\"refresh\" content=\"0;URL=home.php\">";	
}

if(!empty($_SESSION['errre'])){
$errre = $_SESSION['errre'];
echo  "<script type='text/javascript'>alert('$errre');</script>";
$_SESSION['errre']=null;
}


?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<script>
function myFunction() {
    location.reload();
}
</script>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Coding Cage : Login</title>
<link href="bootstrap/css/bootstrap.min.css" rel="stylesheet" media="screen">
<link href="bootstrap/css/bootstrap-theme.min.css" rel="stylesheet" media="screen">
<link rel="stylesheet" href="style.css" type="text/css"  />
</head>
<body>

<div class="signin-form">

	<div class="container">
     
        
       <form class="form-signin" method="post" id="login-form" action="vord.php?job=15">
      
        <h2 class="form-signin-heading">Log In to WebApp. <?php echo  $_SESSION['pa']; ?>  </h2><hr />
        

        <div class="form-group">
        <input type="text" class="form-control" name="username" placeholder="Your Username" required />
        <span id="check-e"></span>
        </div>
        
        <div class="form-group">
        <input type="password" class="form-control" name="password" placeholder="Your Password" />
        </div>
     <img src="captcha.php"><button onclick="myFunction()" class="btn btn-default" >try agin</button>
       <br><br>
         <div class="form-group">
      <input type="text" class="form-control" name="capch" style="width: 48%"  />
        </div>
        <div class="form-group">
            <button type="submit" name="capha" class="btn btn-default">
                	<i class="glyphicon glyphicon-log-in"></i> &nbsp; SIGN IN
            </button>
            

        </div>  
      	<br />
            <label>Don't have account yet ! <a href="sign-up.php">Sign Up</a></label>
      </form>

    </div>
    
</div>

</body>
</html>