<?php
require_once('public.php');

if(!empty($_SESSION['errre'])){
$errre = $_SESSION['errre'];
echo  "<script type='text/javascript'>alert('$errre');</script>";
$_SESSION['errre']=null;
}

?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<script>
function myFunction() {
    location.reload();
}
</script>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Coding Cage : Sign up</title>
<link href="bootstrap/css/bootstrap.min.css" rel="stylesheet" media="screen">
<link href="bootstrap/css/bootstrap-theme.min.css" rel="stylesheet" media="screen">
<link rel="stylesheet" href="style.css" type="text/css"  />
</head>
<body>

<div class="signin-form">

<div class="container" align="center">
    	
        <form class="form-signin" method="post" action="vord.php?job=12">
            <h2 class="form-signin-heading"> Register to WebApp. <?php echo  $_SESSION['pa']; ?>  </h2><hr />
           
            <div class="form-group">
            <input type="text" class="form-control" name="username" placeholder="Enter Username" style="width: 80%" />
            </div>
            <div class="form-group">
            <input type="text" class="form-control" name="email" placeholder="Enter E-Mail ID"  style="width: 80%" />
            </div>
            <div class="form-group">
            	<input type="password" class="form-control" name="password" placeholder="Enter Password" style="width: 80%"  />
            </div>
            <div class="form-group">
                <img src="captcha.php" /><button onclick="myFunction()" class="btn btn-default" >try agin</button><br><br>
            	<input type="text" class="form-control" name="capch" placeholder="Enter capch" style="width: 80%"  />
            	
            </div>
            <div class="clearfix"></div><hr />
            <div class="form-group">
            	<button type="submit" class="btn btn-primary" name="btn-signup">
                	<i class="glyphicon glyphicon-open-file"></i>&nbsp;SIGN UP
                </button>
            </div>
            <br />
            <label>have an account ! <a href="index.php">Sign In</a></label>
        </form>
       </div>
</div>

</div>

</body>
</html>